#!/usr/bin/ksh
#===================================================================================#
# Script:       cdfEditor (cdfeditor.ksh)                                           #
# Function:     This script is used to help the user find the CDF files in the vob, #
#		select from the list of CDF files, check out and check in, and also #
#		promote the CDF file to desired state.                              #			
# Author:       John Paul P. Doria                                                  #
# Date: 	June 28, 2013                                                       #
#===================================================================================#

date=`date +%b%d%y_%H%M%S`
tmp_path=/tmp/.${LOGNAME}_${date}.tmp_1
tmp_basename=/tmp/.${LOGNAME}_${date}.tmp_2
tmp_cdf=/tmp/.${LOGNAME}_${date}.tmp_3

#
# Deletes all temp files if CTRL-C signal, etc is received.
#
trap "echo \"Interrupted.\"; rm -f $tmp_path $tmp_basename $tmp_cdf; exit" 1 2 3 15

#
# Finds the files with *.cdf extension inside the vob.
#
searchCDF() {
	find /ccvobs/$vob -name "*cdf" | wc -l
}

#
# Compares the original and modified file. This is a checkpoint whether a file has been changed or not
# so that ClearCase will check the file in. Otherwise, if the files are identical, ClearCase will
# just cancel the checkout.
#
trigger() {
	cleartool diff $cdfFile $tmp_cdf | grep -c identical
}

#
# This function is responsible for the clearing of temp files.
#
cleanUp() {
	rm -f $tmp_path $tmp_basename $tmp_cdf; exit
}

#
# Created for Line 142: echo "\nEnter a value from 1-`maxValue`, 0 to cancel: \c""
#
maxValue() {
	grep -c cdf $tmp_basename
}

#
# This function gets the max count of lines of $tmp_basename file.
#
maxLine() {
	cat $tmp_basename | wc -l
}

#
# cdfEditor requires the user to be set to a view. If a view is found,
# this function will execute cdfEditor function.
#
checkView() {
	if [ `cleartool pwv | grep "Set view" | grep -c NONE` -eq 1 ]; then
			echo "Error: cdfEditor: You need to be set to a view."
			exit 1
	fi
}

#
# promoteFile function works only after providing "y" to promoteAsk.
# It will then proceed in promoting the file to user's desired state and it will
# execute the cleanUp function afterwards.
#
promoteFile() {
	echo "Please enter the target state for `sed -n \"${value}p\" $tmp_basename`: \c"
	read state
	if [ -z $state ]; then
		echo "Error: the field is empty."
		sleep 1
		break
		promoteFile
	else
		promote -q -t $state $cdfFile && cleanUp
	fi
}

#
# This promoteAsk function works only right after saving the changes on the CDF file.
# This will just ask the user if he wants to promote the file or not. If yes, the promoteFile
# will execute. Otherwise, the script will just terminate and clear all the temp files.
# 
promoteAsk() {
	echo "Do you want to promote this file? [y/n] \c"
	read answer
	answer=`echo $answer | tr [A-Z] [a-z]`
	if [ -z $answer ]; then
		echo "Error: cdfEditor: The field is empty."
		sleep 1
		break
		promoteAsk
	else
		case $answer in
			y) promoteFile; exit 0;;
			n) cleanUp; exit 0;;
			*) echo "Error: cdfEditor: Incorrect option."; sleep 1; promoteAsk;;
		esac
	fi
}

#
# This is the main function called cdfEditor.
# This basically finds the files with .cdf exstension inside the particular vob provided by the user.
# After the files are found, this will be displayed in a list so that the user would be able to choose
# a CDF he wants to work with. The file will be automatically checked out from ClearCase and the vi editor
# will appear. If there are no changes done in the file, this will be unchecked out. If there are changes,
# the file will be automatically checked into ClearCase and promoteAsk function will execute.
#
cdfEditor() {
	echo "CDF Editor v1.0"
	echo "Please enter the VOB name: \c"
	read vob
	
	vob=`echo $vob | tr [A-Z] [a-z]`
	if [ -z $vob ]; then
		echo "Error: cdfEditor: The field is empty."
		sleep 1
		break
		cdfEditor
	else
		UCvob=`echo $vob | tr [a-z] [A-Z]`
		echo "Searching for CDF file(s)..."
		if [ `searchCDF` -eq 0 ]; then
			echo "Error: cdfEditor: No CDF file(s) found."
			cleanUp && exit 1
		else
			find /ccvobs/$vob -type f -name "*cdf" >> $tmp_path
			for file in `cat $tmp_path`; do
				basename "$file" >> $tmp_basename
			done
			if [ `maxLine` -ge 1 ]; then
				echo "Select a CDF file of $UCvob you want to work with:\n"
				lineNum=0
				while [ $lineNum -ne `maxLine` ]; do
					lineNum=`expr $lineNum + 1`
					echo "[$lineNum] `sed -n \"${lineNum}p\" $tmp_basename`"
				done
			fi
		fi
		echo "\nEnter a value from 1-`maxValue`, 0 to cancel: \c"
		read value
		if [ -z $value ] || [ $value -gt `maxValue` ]; then
			echo "\nError: cdfEditor: Could not determine the CDF file."
			cleanUp
			exit 1
		elif [ $value -eq 0 ]; then
			cleanUp
		else
			cdfFile=`sed -n "${value}p" $tmp_path`
			echo "Opening `sed -n \"${value}p\" $tmp_basename`..."
			cp "$cdfFile" $tmp_cdf
			export WR_PROMPT=off && cleartool co -nc "$cdfFile"
			sleep 1
			vi "$cdfFile"
			if [ `trigger` -eq 1 ]; then
				echo "cleartool: Error: By default, won't create version with date identical to predecessor."
				echo "cleartool: Error: Unable to check in \"$cdfFile\"."
				export WR_PROMPT=on && cleartool unco -rm $cdfFile && cleanUp
			else
				cleartool ci -c "Updated CDF file - ${LOGNAME}, $date" "`sed -n \"${value}p\" $tmp_path`"
				export WR_PROMPT=on
				promoteAsk
			fi
		fi
	fi
}

#
# Order of executions.
#
checkView
cdfEditor
cleanUp
