# About #
This script helps the user find the CDF files in the vob, select from the list, check it out, and check it in. It also promotes the CDF file to a desired state.

# Usage #
```
#!bash
./cdfeditor.ksh
```
